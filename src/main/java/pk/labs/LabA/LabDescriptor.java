package pk.labs.LabA;
public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Display.DisplayImpl";
    public static String controlPanelImplClassName = "pk.labs.LabA.controlpanel.ControlPanelImpl";
    
    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.Roomba";
    public static String mainComponentImplClassName = "pk.labs.LabA.roomba.RoombaImpl";
    
    
    
    // endregion

    // region P2
    public static String mainComponentBeanName = null;
    public static String mainFrameBeanName = null;
    // endregion

    // region P3
    public static String sillyFrameBeanName = null;
    // endregion
}
